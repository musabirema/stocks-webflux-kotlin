package rw.mmusabirema.payments.dto

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id


@Data
@NoArgsConstructor
@AllArgsConstructor
class PaymentDto(
    val id: Long,
    val amountPaid: Double,
    val paymentMethod: String,
    val currency: String,
    val reason: String,
    var status: String,
) {


}