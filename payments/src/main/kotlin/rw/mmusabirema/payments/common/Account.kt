package rw.mmusabirema.payments.common

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor

@Data
@AllArgsConstructor
@NoArgsConstructor
class Account(
    val id: Long,
    val username: String,
    val email: String,
    val address: String
) {
}