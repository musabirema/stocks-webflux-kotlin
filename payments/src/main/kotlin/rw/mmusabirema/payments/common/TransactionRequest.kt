package rw.mmusabirema.payments.common

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import rw.mmusabirema.payments.model.Payment

@Data
@AllArgsConstructor
@NoArgsConstructor
class TransactionRequest(
    val payment: Payment,
    val account: Account
) {
}