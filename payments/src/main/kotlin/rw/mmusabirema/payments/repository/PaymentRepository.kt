package rw.mmusabirema.payments.repository

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import rw.mmusabirema.payments.model.Payment

//@Repository
//interface PaymentRepository: JpaRepository<Payment, Long>, ElasticsearchRepository<Payment, Long> {
//}

@Repository
interface PaymentRepository: JpaRepository<Payment, Long>{}