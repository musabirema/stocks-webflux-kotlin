package rw.mmusabirema.payments.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import rw.mmusabirema.payments.model.Payment
import rw.mmusabirema.payments.service.PaymentService

@RestController
@RequestMapping("/api/payments")
class PaymentController {

    @Autowired
    lateinit var paymentService: PaymentService

    @PostMapping
    fun makePayment(@RequestBody transaction: Payment): Mono<Payment> {
        return paymentService.makePayment(transaction)
    }
    @GetMapping
    fun getAllPayments(): Flux<Payment> {
       return paymentService.getAllPayments()
    }
    @PostMapping("/test")
    fun testPayment(@RequestBody transaction: Payment): Mono<Payment> {
       return paymentService.testingCall(transaction)
    }

}