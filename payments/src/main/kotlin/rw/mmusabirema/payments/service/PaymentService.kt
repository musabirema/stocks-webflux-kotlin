package rw.mmusabirema.payments.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import rw.mmusabirema.payments.model.Payment
import rw.mmusabirema.payments.repository.PaymentRepository


@Service
class PaymentService {

    var logger: Logger = LoggerFactory.getLogger(PaymentService::class.java)



    @Autowired
    lateinit var paymentRepository: PaymentRepository

//    fun addNewTransaction(transaction: TransactionRequest): Mono<TransactionResponse>? {
//
//        val account = webClient.build()
//            .post()
//            .uri("http://localhost:8080/api/accounts")
//            .body(Mono.just(transaction), TransactionRequest::class.java)
//
//        paymentRepository.save(transaction)
//
//        return null
//
//    }

    val webClient = WebClient.create("http://localhost:8080/api/accounts")


    fun makePayment(transaction: Payment): Mono<Payment> {
        return paymentRepository.save(transaction).toMono()
    }

    fun testingCall(transaction: Payment): Mono<Payment>{
//        var customer :Mono<Payment> = webClient.post()
//            .uri("http://localhost:8080/api/accounts")
//            .body(Mono.just(transaction), Payment::class.java)
//            .retrieve()
//            .bodyToMono(Payment::class.java)
//        println(customer.toFlux())
//
//        val accounts: Flux<*> =
//            webClient.get().uri("http://account-service/customer/{customer}", id).retrieve().bodyToFlux(
//                Account::class.java
//            )
//        return accounts
//            .collectList()
//            .map { a: Any? -> Customer(a) }
//            .mergeWith(repository.findById(id))
//            .collectList()
//            .map(CustomerMapper::map)

        return paymentRepository.save(transaction).toMono()
    }

    fun getAllPayments(): Flux<Payment> {
        logger.info("Payments returned")
        return paymentRepository.findAll().toFlux()
    }




}