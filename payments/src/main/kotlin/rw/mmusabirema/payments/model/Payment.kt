package rw.mmusabirema.payments.model

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
class Payment(
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    val id: Long,
    val amountPaid: Double,
    val paymentMethod: String,
    val currency: String,
    val reason: String,
    var status: String,
) {


}