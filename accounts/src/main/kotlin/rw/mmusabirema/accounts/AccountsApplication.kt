package rw.mmusabirema.accounts

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.loadbalancer.LoadBalanced
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.client.WebClient


@SpringBootApplication
@EnableEurekaClient
class AccountsApplication

fun main(args: Array<String>) {
	runApplication<AccountsApplication>(*args)
}

@Bean
@LoadBalanced
fun loadBalancedWebClientBuilder(): WebClient.Builder? {
	return WebClient.builder()
}