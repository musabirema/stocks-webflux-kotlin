package rw.mmusabirema.accounts.service

import lombok.extern.log4j.Log4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import rw.mmusabirema.accounts.model.Account
import rw.mmusabirema.accounts.repository.AccountRepository
import java.util.*


@Service
@Log4j
class AccountService {

    @Autowired
    lateinit var accountRepository: AccountRepository

    var logger: Logger = LoggerFactory.getLogger(AccountService::class.java)


    /**
     * Registering new account
     * @params customerName, customerEmail, customerBankAccount, customerAddress
     * */
    fun registerAccount(account: Account): Mono<Account> {
        if (isAccountExists(account.id)){
            logger.error("Error: Accounts ${account.id} already exists ")
            throw RuntimeException("Error: the account already exists!")

        }else{
            logger.info("Account ${account.customerBankAccount} successfully created")
            return accountRepository.save(account).toMono()

        }
    }

    /**
     * Returning all accounts available
     * @param none
     * */
    fun getAllAccounts(): Flux<Account> {
        return accountRepository.findAll().toFlux()
    }

    /**
     * Returning the individual account
     * @param id
     * */
    fun getSingleAccount(id: Long): Mono<Optional<Account>> {
        if (isAccountExists(id)) {
            return accountRepository.findById(id).toMono()
        }else{
            logger.error("The account of id $id is not found")
            throw RuntimeException("The account of id $id is not found")
        }
    }

    /**
     * Checking if the account exists
     * @param id
     * @return boolean
     * */

    fun isAccountExists(id: Long): Boolean{
        var existingAccount = accountRepository.findById(id)
        return existingAccount.isPresent
    }


}