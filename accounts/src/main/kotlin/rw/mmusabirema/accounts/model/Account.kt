package rw.mmusabirema.accounts.model

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
class Account(
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    val id: Long,
    val customerName: String,
    val customerBankAccount: String,
    val customerEmail: String,
    val customerAddress: String
) {
}