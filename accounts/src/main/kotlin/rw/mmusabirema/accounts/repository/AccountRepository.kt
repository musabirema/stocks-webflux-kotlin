package rw.mmusabirema.accounts.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import rw.mmusabirema.accounts.model.Account

@Repository
interface AccountRepository: JpaRepository<Account, Long>{
    //TODO("Adding more custom queries")
}