package rw.mmusabirema.accounts.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import rw.mmusabirema.accounts.service.AccountService
import rw.mmusabirema.accounts.model.Account
import java.util.*

@RestController
@RequestMapping("/api/accounts")
class AccountController {
    @Autowired
    lateinit var accountService: AccountService

    @PostMapping
    fun addNewAccount(@RequestBody account: Account): Mono<Account> {
        return accountService.registerAccount(account)
    }

    @GetMapping
    fun getAllAccounts(): Flux<Account> =  accountService.getAllAccounts()

    @GetMapping("/{id}")
    fun getSingleAccount(@PathVariable id: Long): Mono<Optional<Account>> {
        return accountService.getSingleAccount(id)
    }
}