package rw.mmusabirema.accounts.dto

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor

@Data
@AllArgsConstructor
@NoArgsConstructor
class AccountDto(
    val id: Long,
    val customerName: String,
    val customerBankAccount: String,
    val customerEmail: String,
    val customerAddress: String
) {
}