package rw.mmusabirema.accounts.dto

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor

@Data
@NoArgsConstructor
@AllArgsConstructor
class PaymentDto(
    val id: Long,
    val amountPaid: Double,
    val paymentMethod: String,
    val currency: String,
    val reason: String,
    val account: Long,
    var status: String,
) {


}